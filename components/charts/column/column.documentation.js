import * as description from './column.md';
import examples from './examples';

export default {
  description,
  examples,
};
