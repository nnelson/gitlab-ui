import * as description from './form_radio.md';
import examples from './examples/form_radio';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-radio',
};
